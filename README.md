# Actavis Hub #

Installing Laravel

Laravel utilizes Composer to manage its dependencies. 
So, before using Laravel, make sure you have Composer installed on your machine.

Via Laravel Installer

First, download the Laravel installer using Composer:

--> composer global require "laravel/installer"

# Local Development Server #

If you have PHP installed locally and you would like to use PHP's built-in development server to serve your application, you may use the serve Artisan command. This command will start a development server at http://localhost:8000:

--> php artisan serve